# Hermin

A lighter version of [Track3's Hermit theme](https://github.com/Track3/hermit) for [Hugo](https://gohugo.io)

See Hermit's official GitHub repo for documentation. Unless otherwise stated here, everything is identical.

* Significantly simplified JS
* Removed theme name from footer
* Bottom header does not disappear